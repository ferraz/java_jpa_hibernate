import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class AlunoController {
	
	
	//Abrir uma sessão no banco de dados.
	EntityManagerFactory emf;//Transação
	//Para realizar as operações.
	EntityManager em;
	
	public AlunoController() {
		
		//Passar como parâmetro o nome que foi definido na unidade de persistência do xml.
		emf = Persistence.createEntityManagerFactory("aluno");
		em = emf.createEntityManager();
	}
	
	public void salvar(Aluno _aluno) {
		
		//Iniciando a conexão com banco
		em.getTransaction().begin();
		//Salvando o aluno no banco
		em.merge(_aluno);
		em.getTransaction().commit();
		emf.close();
	}
	
	public void remover(Aluno _aluno) {
		System.out.println(_aluno.getMatricula()+" - matricula");
		em.getTransaction().begin();
		em.getTransaction();
		Query q = em.createNativeQuery("DELETE aluno FROM aluno WHERE matricula ="+_aluno.getMatricula());
		q.executeUpdate();
		em.getTransaction().commit();
		//encerrando a conexão
		emf.close(); 
	}
	
	public List<Aluno> listar(){
		//Abrir uma conexão com o banco
		em.getTransaction().begin();
		//Retornando todos os alunos em forma de objeto
		Query consulta = em.createQuery("SELECT aluno FROM Aluno aluno");
		List<Aluno> lista = consulta.getResultList();
		em.getTransaction().commit();
		emf.close();
		return lista;
	}
	
	
}
