import java.util.List;

public class Teste {

	public static void main(String[] args) {
		
		
		
		Aluno a1 = new Aluno();
		a1.setNome("Tobias Ferraz");
		a1.setIdade(21);
		a1.setMatricula("123456789");
		
		Aluno a2 = new Aluno();
		a2.setNome("Marcleiton Mattos");
		a2.setIdade(25);
		a2.setMatricula("123456666");
		
		AlunoController con = new AlunoController();
		con.salvar(a1);
		con.salvar(a2);
		
		List<Aluno> alunos = con.listar();
		
		for (int i = 0; i < alunos.size(); i++) {
			System.out.println("Nome: " + alunos.get(i).getNome() + 
							   " - Idade: "+alunos.get(i).getIdade() + 
							   " - Matricula: " + alunos.get(i).getMatricula());
		}
		
		
	}

}
